package com.example.shaik.myapplication;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity {

    private MediaPlayer player;
    int no;

    final int[] ids={R.raw.themesong,R.raw.pika};

    ImageView startpika,stoppika;
   ProgressBar progressBar;
 AsyncTaskRunner runner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startpika=(ImageView) findViewById(R.id.start_pika);
        stoppika=(ImageView) findViewById(R.id.stop_pika);
        progressBar=(ProgressBar)findViewById(R.id.progressBar);

        runner=new AsyncTaskRunner();


        startpika.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,MusicService.class);
                intent.putExtra("number",1);
                progressBar.setVisibility(View.VISIBLE);
                //uncomment this if you want to use AsyncTask
               // runner.execute(0);
               startService(intent);
            }
        });

        stoppika.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.GONE);
                stopService(new Intent(MainActivity.this,MusicService.class));
            }
        });



    }

    private  class AsyncTaskRunner extends AsyncTask<Integer, String, String> {



        @Override
        protected String doInBackground(Integer... params) {
            player= MediaPlayer.create(MainActivity.this,ids[params[0]]);
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
            player.setLooping(true);
            player.start();

            return "";
        }


        @Override
        protected void onPostExecute(String result) {

        }


        @Override
        protected void onPreExecute() {

        }


        @Override
        protected void onProgressUpdate(String... text) {

        }
    }

}
