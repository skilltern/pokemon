package com.example.shaik.myapplication;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ProgressBar;

/**
 * Created by shaik on 21-10-2018.
 */

public class MusicService extends Service {
    private MediaPlayer player;
    int no;

    final int[] ids={R.raw.themesong,R.raw.pika};


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle b=new Bundle();
        b=intent.getExtras();
        try{
            no=b.getInt("number",0);}
        catch (Exception e){
            Log.e("error in service",e.toString());}

        player=MediaPlayer.create(this,ids[no]);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        player.setLooping(true);
        player.start();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(player!=null)
        player.stop();
    }
}
